/*!
 * project_shg
 * 
 * 
 * @author Thuclfc
 * @version 2.0.0
 * Copyright 2023. MIT licensed.
 */$(document).ready(function () {
  //show menu
  $('.navbar-toggle').on('click', function () {
    $(this).toggleClass('active');
    $('.navbar-collapse').toggleClass('show');
  });
  $('.navbar-collapse .close').on('click', function () {
    $('.navbar-collapse').removeClass('show');
  });
  // active navbar of page current
  var urlcurrent = window.location.pathname;
  $(".navbar-nav li a[href$='" + urlcurrent + "'],.nav-category li a[href$='" + urlcurrent + "']").addClass('active');
  $(window).on("load", function (e) {
    $(".navbar-nav .sub-menu").parent("li").append("<span class='show-menu'></span>");
  });

  //select custom
  $('.selected').on('click', function () {
    $(this).parent().toggleClass('show');
  });
  $('.select-list li').on('click', function () {
    $(this).parents('.box-select').find('.select-list li').removeClass('active');
    $(this).addClass('active');
    var img_icon = $(this).data('icon');
    var txt = $(this).data('txt');
    var html_img = `<img src="${img_icon}">`;
    $(this).parents('.box-select').removeClass('show');
    $(this).parents('.box-select').find('.selected .icon').html('').append(html_img);
    $(this).parents('.box-select').find('.selected .txt').html('').append(txt);
  });
  $(document).mouseup(function (e) {
    var container = $(".box-select");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
      $('.box-select').removeClass('show');
    }
  });
});